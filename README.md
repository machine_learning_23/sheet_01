# Sheet_01

## Name
Sheet_01

## Description
This is the first exercise sheet of the subject Machine Learning

## Authors
Huynh Tuan Duy Bui, Quang Tran, Sazid Akhter Turzo

## License
The license for Sheet_01 belongs to the Machine Learning subject at institute for informatics of university of Goettingen
